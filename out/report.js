$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/java/com/bdd/co/LoginPage.feature");
formatter.feature({
  "id": "co-application-laogin-feature-testing",
  "description": "",
  "name": "CO application laogin feature testing",
  "keyword": "Feature",
  "line": 1
});
formatter.before({
  "duration": 6692984710,
  "status": "passed"
});
formatter.scenario({
  "id": "co-application-laogin-feature-testing;validate-user-is-able-to-login-the-application",
  "tags": [
    {
      "name": "@Test1",
      "line": 3
    }
  ],
  "description": "",
  "name": "Validate user is able to login the application",
  "keyword": "Scenario",
  "line": 4,
  "type": "scenario"
});
formatter.step({
  "name": "user open the application on browser",
  "keyword": "Given ",
  "line": 6
});
formatter.step({
  "name": "user enters username in textfield",
  "keyword": "And ",
  "line": 7
});
formatter.step({
  "name": "user enters password in passwordbox",
  "keyword": "And ",
  "line": 8
});
formatter.step({
  "name": "user clicks on login button",
  "keyword": "And ",
  "line": 9
});
formatter.match({
  "location": "CommonSteps.openApplication()"
});
formatter.result({
  "duration": 2576680745,
  "status": "passed"
});
formatter.match({
  "location": "LoginPageStep.enter_user()"
});
formatter.result({
  "duration": 207410655,
  "status": "passed"
});
formatter.match({
  "location": "LoginPageStep.enter_pass()"
});
formatter.result({
  "duration": 103874663,
  "status": "passed"
});
formatter.match({
  "location": "LoginPageStep.click_submit()"
});
formatter.result({
  "duration": 566777151,
  "status": "passed"
});
formatter.after({
  "duration": 581998993,
  "status": "passed"
});
});
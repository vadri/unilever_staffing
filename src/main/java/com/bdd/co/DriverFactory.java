package com.bdd.co;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class DriverFactory {

	protected static WebDriver driver = new FirefoxDriver();
	

	public DriverFactory() {
		initialize();
	}

	public void initialize() {
		if (driver == null)

			createNewDriverInstance();
	}

	public void createNewDriverInstance() {
	//System.setProperty("webdriver.firefox.marionette","C:\\geckodriver.exe");
		driver = new FirefoxDriver();
	}

	public WebDriver getDriver() {
		return driver;
	}

	public void destroyDriver() {
		driver.quit();
		driver = null;
	}
}

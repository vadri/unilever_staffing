package com.bdd.co;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WebConnector {
	
WebDriver driver;
	
	public WebConnector(WebDriver driver){
		
		this.driver = driver;
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		PageFactory.initElements(driver, this);
	}

	@FindBy(name="userName")
	private WebElement username;
	
	@FindBy(name="password")
	private WebElement password;
	
	@FindBy(name="login")
	private WebElement loginbutton;
	
	
public void launch(){
		
		driver.get("http://newtours.demoaut.com");
		
	}
	
	public void usn() throws Exception{
        Thread.sleep(5000);
		username.sendKeys("tutorial");
	}
	
	public void pass() throws Exception{
		Thread.sleep(5000);
		password.sendKeys("tutorial");
	}
	
	public void submit()throws Exception{
		Thread.sleep(5000);
		loginbutton.click();
	}
}

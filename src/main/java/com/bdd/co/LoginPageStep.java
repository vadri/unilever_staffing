package com.bdd.co;


import cucumber.api.java.en.And;

public class LoginPageStep extends DriverFactory {

	
	@And("^user enters username in textfield$")
	public void enter_user() throws Exception{
		new WebConnector(driver).usn();
	}
   @And("^user enters password in passwordbox$")
   public void enter_pass() throws Exception{
	new WebConnector(driver).pass();
}
	@And("^user clicks on login button$")
	public void click_submit()throws Exception{
		new WebConnector(driver).submit();
	}

}

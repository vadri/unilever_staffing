package com.bdd.co;


import java.util.concurrent.TimeUnit;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;

public class CommonSteps extends DriverFactory {
	

	
	@Before
	public void openBrowser(){
		
		driver.manage().window().maximize();
		
	}
	
	@After
	public void destroy()throws Exception{
		Thread.sleep(7000);
		new DriverFactory().destroyDriver();
	}

	@Given("^user open the application on browser$")
	public void openApplication(){
		new WebConnector(driver).launch();
	}
	


}
